var app = require('../index');
var request = require("request");
var expect  = require("chai").expect;

describe("Suma", function() {
  var url = "http://localhost:3000/suma/1/1";

  it("El servicio está ok", function() {
    request(url, function(error, response, body) {
      expect(response.statusCode).to.equal(200);
    });
  });

  it("Resultado de la suma es 2", function() {
    request(url, function(error, response, body) {
      expect(body).to.equal("{\"suma\":2}");
    });
  });

});

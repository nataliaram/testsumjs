const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Hello World!'))

app.get('/suma/:num1/:num2', function(req, res) {
  num1 = parseInt(req.params.num1);
  num2 = parseInt(req.params.num2);

  suma = num1 + num2;

  res.send({suma});
})

app.listen(port, () => console.log(`Listening on port ${port}!`))
